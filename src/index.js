import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

class Task extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			content: this.props.content,
			temp_text: this.props.content,
			is_done: false,
			is_updating: false
		}
		this.done = this.done.bind(this);
		this.update = this.update.bind(this);
		this.change = this.change.bind(this);
		this.save = this.save.bind(this);
		this.cancel = this.cancel.bind(this);
		this.delete = this.delete.bind(this);
	}

	done(e) {
		this.setState(prevState => ({
			content: <strike>{prevState.content}</strike>,
			is_done: true
		}));
	}

	update(e) {
		this.setState(prevState => ({
			is_updating: true
		}));
	}

	change(e) {
		this.setState({temp_text: e.target.value});
	}

	save(e) {
		//Update data from backend
		this.setState(prevState => ({
			content: prevState.temp_text,
			is_updating: false
		}));
	}

	cancel(e) {
		this.setState(prevState => ({
			temp_text: this.state.content,
			is_updating: false
		}));
	}

	delete(e) {
		// to call method delete from TaskList
		this.props.onDelete(this.props.task_id);
	}

	render() {
		return (
			<div className="task">
				{this.state.is_updating ? (
					<div>
						<input onChange={this.change} value={this.state.temp_text} />
						<button onClick={this.save}>Save</button>
						<button onClick={this.cancel}>Cancel</button>
					</div>
				):(
					<div>
						<div>{this.state.content}</div>
						{this.state.is_done ? (<div><button onClick={this.delete}>Delete</button></div>):(
							<div>
								<button onClick={this.update}>Update</button>
								<button onClick={this.delete}>Delete</button>
								<button onClick={this.done}>Done</button>
							</div>
						)}
					</div>
				)}
			</div>
		);
	}
}

class TaskList extends React.Component {
	constructor(props) {
		super(props);
		this.delete = this.delete.bind(this);
	}

	delete(task_id) {
		// to call method delete from todoApp
		this.props.onDelete(task_id);
	}

	render() {
		return (
			<div className="task-list">
				<ul>
					{this.props.tasks.map(task => (<li key={task.id}><Task onDelete={this.delete} task_id={task.id} content={task.content}/></li>))}
				</ul>
			</div>
		);
	}
}

class TodoApp extends React.Component {
	constructor(props) {
	    super(props);
	    this.state = {
	    	tasks: [],
	    	text: ""
	    };
	    // This binding is necessary to make `this` work in the callback
	    this.add = this.add.bind(this);
	    this.change = this.change.bind(this);
	    this.delete_task = this.delete_task.bind(this);
	  }

	add() {
		if (this.state.text) {
			this.setState(prevState => ({
				tasks: prevState.tasks.concat({content: prevState.text, id: Date.now()}),
				text: ""
			}));
		}
	}

	change(e) {
		this.setState({text: e.target.value});
	}

	delete_task(task_id) {
		var is_confirmed = window.confirm("Are you sure to delete the task ?");
		if(is_confirmed) {
			//Delete record in backend
			//Remove item in tasks_list
			var new_tasks = this.state.tasks;
			for(var i = 0; i < new_tasks.length; i++){
				if(new_tasks[i].id === task_id){
					new_tasks.splice(i, 1);
				}
			}
			this.setState({tasks: new_tasks});
		}
	}

	render() {
		return (
			<div className="submit-bar">
				<input id="aContent" onChange={this.change} placeholder="Input task" value={this.state.text} />
				<button onClick={this.add}>Add</button>
				<TaskList onDelete={this.delete_task} tasks={this.state.tasks} />
			</div>
		);
	}
}

ReactDOM.render(
  <TodoApp />,
  document.getElementById('root')
);